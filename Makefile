
SOURCES = ./

lint:
	./node_modules/.bin/eslint $(SOURCES)

lint-fix:
	./node_modules/.bin/eslint $(SOURCES) --fix

.PHONY: lint