'use strict';

const WrapperComponentBase = require('../../lib/WrapperComponentBase');

/*
 * This is a Catberry Cat-component file.
 * More details can be found here
 * http://catberry.org/documentation#cat-components-interface
 */

class Registration extends WrapperComponentBase {

	/**
	 * Creates a new instance of the "registration" component.
	 */
	constructor() {
		super();
	}

	/**
	 * Gets a data context for the template engine.
	 * This method is optional.
	 * @returns {Promise<Object>|Object|null|undefined} The data context for the template engine.
	 */
	render() {
		var currentContext = this.$context;
		return currentContext.getStoreData();

/*			.then(function(user){
				if(user.status==true){
					currentContext.redirect('/');
				}
			})*/
	}

	/**
	 * Returns event binding settings for the component.
	 * This method is optional.
	 * @returns {Promise<Object>|Object|null|undefined} The binding settings or nothing.
	 */
	bind() {
		return {
			submit: {
				'form#registration-form': this.registrationAction
			},
			click: {
				'#user_logout': super.logoutAction
			}
		};
	}

	/**
	 * Clans everything up. The events have been set by .bind() method are cleaned automatically.
	 * This method is optional.
	 * @returns {Promise|undefined} Promise or finished work or nothing.
	 */
	unbind() {

	}

	registrationAction(e) {
		super.authAction(e, 'registration-action');
	}
}

module.exports = Registration;

