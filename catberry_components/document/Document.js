'use strict';

/*
 * This is a Catberry Cat-component file.
 * More details can be found here
 * http://catberry.org/documentation#cat-components-interface
 */

class Document {

	constructor() {
		const hb = this.$context.locator.resolve('handlebars');
		hb.registerHelper('if_eq', (a, b, options) => {
			if (a === b) {
				return options.fn(this);
			}
			return options.inverse(this);
		});
	}
}

module.exports = Document;

