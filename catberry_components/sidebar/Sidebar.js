'use strict';

const ComponentBase = require('../../lib/ComponentBase');

/*
 * This is a Catberry Cat-component file.
 * More details can be found here
 * http://catberry.org/documentation#cat-components-interface
 */

class CommonMenu extends ComponentBase {

	/**
	 * Creates a new instance of the "common-menu" component.
	 */
	constructor() {
		super();
	}

	/**
	 * Gets a data context for the template engine.
	 * This method is optional.
	 * @returns {Promise<Object>|Object|null|undefined} The data context for the template engine.
	 */
	render() {
		return this.$context.getStoreData()
			.then(data => {
				return data;
			});
	}

	/**
	 * Returns event binding settings for the component.
	 * This method is optional.
	 * @returns {Promise<Object>|Object|null|undefined} The binding settings or nothing.
	 */

	bind() {
		var img = document.querySelector('img#avatar');

		if (typeof img !== 'undefined' && img !== null) {
			this.processGradient(img);
		}

		return {
			click: {
				'#userLogout': super.logoutAction,
				'#refreshMapFromHashListener': this.refreshMap
			}
		};
	}

	/**
	 * Clans everything up. The events have been set by .bind() method are cleaned automatically.
	 * This method is optional.
	 * @returns {Promise|undefined} Promise or finished work or nothing.
	 */
	unbind() {

	}

	refreshMap(e) {
		e.preventDefault();
		var map = this.$context.getComponentById('map');
		var dgMap = map.getMap();

		var data = map.getMapCenter();
		dgMap.setView(data.center, data.zoom);
		$('#showToMap').popover('hide');
		map.offcanvasHide();
	}

	processGradient(img) {
		var rgb, d, n, l, lr, p, g;
		var style = '';
		var prefixes = ['-o-', '-ms-', '-moz-', '-webkit-', ''];
		var colorThief = new ColorThief();

		img.crossOrigin = 'anonymous';

		try {
			rgb = colorThief.getColor(img);
		} catch (e) {
			rgb = [177, 83, 21];
		}

		n = this.rgbToHex(rgb);
		d = this.colorLuminance(n, -0.1);
		l = this.colorLuminance(n, 0.1);
		lr = this.colorLuminance(n, 0.2);

		for (p of prefixes) {
			style = style.concat('background:', p, 'linear-gradient(top left, ', d, ' 25%, ', n, ' 50%, ', l, ' 75%, ', lr, ' 100%);');
		}

		g = document.querySelector('.navmenu-header');
		g.style = style;
	}

	/**
	 * hex — a hex color value such as “#abc” or “#123456” (the hash is optional)
	 * lum — the luminosity factor, i.e. -0.1 is 10% darker, 0.2 is 20% lighter, etc.
	 */
	colorLuminance(hex, lum) {
		// validate hex string
		hex = String(hex).replace(/[^0-9a-f]/gi, '');
		if (hex.length < 6) {
			hex = hex[0] + hex[0] + hex[1] + hex[1] + hex[2] + hex[2];
		}
		lum = lum || 0;

		// convert to decimal and change luminosity
		var rgb = '#';
		var c, i;
		for (i = 0; i < 3; i++) {
			c = parseInt(hex.substr(i * 2, 2), 16);
			c = Math.round(Math.min(Math.max(0, c + (c * lum)), 255)).toString(16);
			rgb += (`00${c}`).substr(c.length);
		}
		return rgb;
	}

	rgbToHex(rgb) {
		var r, g, b;

		r = rgb[0].toString(16);
		g = rgb[1].toString(16);
		b = rgb[2].toString(16);

		if (r.length === 1) {
			r = `0${r}`;
		}
		if (g.length === 1) {
			g = `0${g}`;
		}
		if (b.length === 1) {
			b = `0${b}`;
		}

		return `#${r}${g}${b}`;
	}

}

module.exports = CommonMenu;

