'use strict';

/*
 * This is a Catberry Cat-component file.
 * More details can be found here
 * http://catberry.org/documentation#cat-components-interface
 */

class CityMenu {

	/**
	 * Creates a new instance of the "city-menu" component.
	 */
	constructor() {

	}

	/**
	 * Gets a data context for the template engine.
	 * This method is optional.
	 * @returns {Promise<Object>|Object|null|undefined} The data context for the template engine.
	 */
	render() {
		return this.$context.getStoreData();
	}

	/**
	 * Returns event binding settings for the component.
	 * This method is optional.
	 * @returns {Promise<Object>|Object|null|undefined} The binding settings or nothing.
	 */
	bind() {
		return {
			input: {
				'#city_search': this.cityFilter
			},
			click: {
				'.conditions': this.chooseCity
			}
		};
	}

	chooseCity(e) {
		e.preventDefault();

		var currentElement;

		if (e.target.tagName === 'LI') {
			currentElement = e.target.children[0];
		} else {
			currentElement = e.target.parentNode.children[0];
		}

		var lat = currentElement.dataset.lat;
		var lon = currentElement.dataset.lon;

		if (lat !== 'undefined' && lon !== 'undefined') {
			this.$context.locator.resolve('window').location.hash = `#12/${lat}/${lon}`;
			this.$context.location.fragment = `12/${lat}/${lon}`;
			var cityElement = document.getElementsByClassName('city-current');
			cityElement[0].innerHTML = currentElement.innerHTML;

			$('#showToMap').popover(
				{
					placement: 'top',
					html: true
				}
			);
			$('#showToMap').popover('show');
		}
	}

	// Фильтрация городов на jquery
	cityFilter(e) {
		var typingTimer;                // timer identifier
		var doneTypingInterval = 80;
		var input = $(e.target);
		input.on('keyup input', function() {
			var searchCondition = $(this).val();
			if (searchCondition.length >= 1) {
				clearTimeout(typingTimer);
				typingTimer = setTimeout(() => {
					$('.conditions').each(function() {
						$(this).hide().filter(function() {
							var conditionString = $(this).children('a').html().toString();
							var textSeach = conditionString.toLowerCase().search(input.val().toLowerCase()) !== -1;
							return textSeach;
						}).show();
					});
				}, doneTypingInterval);
			} else {
				$('.conditions').each(function() {
					$(this).show();
				});
			}
		});
	}

	/**
	 * Clans everything up. The events have been set by .bind() method are cleaned automatically.
	 * This method is optional.
	 * @returns {Promise|undefined} Promise or finished work or nothing.
	 */
	unbind() {

	}
}

module.exports = CityMenu;

