'use strict';

const WrapperComponentBase = require('../../lib/WrapperComponentBase');

/*
 * This is a Catberry Cat-component file.
 * More details can be found here
 * http://catberry.org/documentation#cat-components-interface
 */

class UserSettings extends WrapperComponentBase {

	/**
	 * Creates a new instance of the "placemark-show" component.
	 */
	constructor() {

		super();

	}

	/**
	 * Gets a data context for the template engine.
	 * This method is optional.
	 * @returns {Promise<Object>|Object|null|undefined} The data context for the template engine.
	 */
	render() {
		return this.$context.getStoreData();
	}

	/**
	 * Returns event binding settings for the component.
	 * This method is optional.
	 * @returns {Promise<Object>|Object|null|undefined} The binding settings or nothing.
	 */
	bind() {
		return {
			submit: {
				'#user-settings': this.save
			},
			change: {
				'#inputImage': this.photoProfile
			},
			click: {
				'.success-crop': this.downloadPhoto,
			}
		};
	}

	makeRequest(url, data) {
		return new Promise(function (resolve, reject) {
		    var xhr = new XMLHttpRequest();
		   	xhr.open('POST', url, true);

			xhr.onload = function () {
				if (this.status >= 200 && this.status < 300) {	
					resolve(xhr.response);
				} else {
					reject({
						status: this.status,
						errors: xhr.errors
					});
				}
			};

		    xhr.onerror = function () {
				reject({
					status: this.status,
					errors: xhr.errors
				});
		    };
		    xhr.send(data);
		});
	}

	downloadPhoto(e){
		e.preventDefault();
		var current_cnt = this.$context;
		var authToken = this.$context.cookie.get('auth_key');

		var x0 = $('.success-crop').data('x0');
		var y0 = $('.success-crop').data('y0');
		var x1 = $('.success-crop').data('x1');
		var y1 = $('.success-crop').data('y1');

		/* COORDINATES FOR SEND */
		//console.log( x0, y0, x1, y1);

		var file = document.getElementById('inputImage').files[0];
		var formData = new FormData();
		formData.append('file[4]', file);
		formData.append('x0', x0);
		formData.append('y0', y0);
		formData.append('x1', x1);
		formData.append('y1', y1);

		var xhr = new XMLHttpRequest();
		var config = this.$context.locator.resolve('config');
		var authKey = this.$context.cookie.get('auth_key');
		var url = `${config.api.url}settingslist/image?access-token=${authKey}`;

		this.makeRequest(url, formData)
			.then(function(data){
				data = JSON.parse(data);
				if(data.status){
					current_cnt.sendBroadcastAction('update-action', {});
					console.log('DEBUG: SAVE OK!');
				}
			});

		//xhr.open('POST', url, true);
		//xhr.send(formData);

	}


	photoProfile(e) {
		e.preventDefault();

		$('.image-viewer').show();
		$('.cropper-enabled').show();

		var options = {
			viewMode: 2,
			checkOrientation: false,
			aspectRatio: 1,
			preview: '.img-preview',
			minCropBoxWidth: 50,
			//cropBoxMovable: false,
		};


		var URL = window.URL || window.webkitURL;
		var files = e.target.files;
		var file;
		var blobURL;

		if (files && files.length) {
		  file = files[0];

		  if (/^image\/\w+$/.test(file.type)) {
			    blobURL = URL.createObjectURL(file);
			    //console.log(blobURL);

			    $('#image').cropper(options).cropper('replace', blobURL);
			    $('#image').on({
			    	'zoom.cropper': function (e) {
			    		console.log(e.ratio);
			    		if(e.ratio > 0.6) {
			    			e.preventDefault();
			    		}
			    	},
			    	'crop.cropper': function (e) {
			    		$('.success-crop').data('x0', parseInt(e.width));
			    		$('.success-crop').data('y0', parseInt(e.height));
			    		$('.success-crop').data('x1', parseInt(e.x));
			    		$('.success-crop').data('y1', parseInt(e.y));
			    	}
			    })
			} else {
				window.alert('Плохой файл.');
			}
		}

	}

	save(e) {
		e.preventDefault();

		var context = this.$context;
		context.sendAction('savesettings-action', super.serializeObject(e.target))
			.then((result) => {
				$('.with-errors .alert').remove();
				if (result.errors !== undefined) {
					for (var i = 0; i < result.errors.length; i++) {
						// console.log(result.errors[i].errors);
						var errorTemplate = $('.error-content .alert');
						for (var error in result.errors[i].errors) {
							if ({}.hasOwnProperty.call(result.errors[i].errors, error)) {
								var alert = errorTemplate.clone();
								$(alert)
									.removeClass('hidden')
									.children('span')
									.text(result.errors[i].errors[error]);
								$(`.with-errors[data-id=${result.errors[i].user_setting_id}]`).append(alert);
							}
						}
					}
				}
			})
			.then(() => {
				context.sendBroadcastAction('update-action', {});
			});

	}

	/**
	 * Clans everything up. The events have been set by .bind() method are cleaned automatically.
	 * This method is optional.
	 * @returns {Promise|undefined} Promise or finished work or nothing.
	 */
	unbind() {

	}
}

module.exports = UserSettings;

