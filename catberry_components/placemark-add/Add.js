'use strict';

const WrapperComponentBase = require('../../lib/WrapperComponentBase');

/*
 * This is a Catberry Cat-component file.
 * More details can be found here
 * http://catberry.org/documentation#cat-components-interface
 */

class PlacemarkAdd extends WrapperComponentBase {

	/**
	 * Creates a new instance of the "placemark-add" component.
	 */
	constructor() {
		super();
		this.files = [];
	}

	/**
	 * Gets a data context for the template engine.
	 * This method is optional.
	 * @returns {Promise<Object>|Object|null|undefined} The data context for the template engine.
	 */
	render() {
		return this.$context.getStoreData();
	}

	/**
	 * Returns event binding settings for the component.
	 * This method is optional.
	 * @returns {Promise<Object>|Object|null|undefined} The binding settings or nothing.
	 */
	bind() {
		// this.map = this.$context.getComponentById('map');
		// this.dgMap = this.map.getMap();
		return {
			change: {
				'input#fileChoose': this.prepare
			},
			submit: {
				'form#uploadForm': this.upload,
				'form#addForm': this.add
			},
			click: {
				'button#clearButton': this.clear,
				'button#viewMap': this.viewMap,
				'button#viewMapLocate': this.viewMapLocate
			}
		};
	}

	/**
	 * Clans everything up. The events have been set by .bind() method are cleaned automatically.
	 * This method is optional.
	 * @returns {Promise|undefined} Promise or finished work or nothing.
	 */
	unbind() {

	}

	viewMap(e) {
		e.preventDefault();
		var map = this.$context.getComponentById('map');
		var dgMap = map.getMap();

		dgMap.setZoom(dgMap._layersMaxZoom);
		map.toogleCoordinatesChooseButton(dgMap._zoom, dgMap._layersMaxZoom);
		$('#map-chooser, #map-dialog').fadeIn(600);
		map.offcanvasHide();
	}

	viewMapLocate(e) {
		e.preventDefault();
		var map = this.$context.getComponentById('map');
		var dgMap = map.getMap();

		dgMap.locate({enableHighAccuracy: true});
		map.toogleCoordinatesChooseButton(dgMap._zoom, dgMap._layersMaxZoom);
		$('#map-chooser, #map-dialog').fadeIn(600);
		map.offcanvasHide();
	}

	add(e) {
		e.preventDefault();
		e.stopPropagation();

		var button = e.target.querySelector('[type=submit]');
		var map = this.$context.getComponentById('map');
		var dgMap = map.getMap();
		var successMesage = 'Ваше граффити добавлено, можете посмотреть его';

		button.setAttribute('disabled', true);
		$(e.target).addClass('form-loading');

		var result = this.$context.sendAction('add-action', super.serializeObject(e.target));
		result.then(data => {
			if (data.status) {
				map.offcanvasHide();
				var marker = DG.marker([data.coordinates.lat, data.coordinates.lng])
					.bindPopup(`${successMesage} <a href="/placemark/show/${data._id.$id}">тут</a>`)
					.addTo(dgMap);
				marker.openPopup();
			} else {
				if (data.hasOwnProperty('errors')) {
					$('.error-content').html('');
					button.removeAttribute('disabled');
					var errorTemplate = $('.alert');
					$(e.target).removeClass('form-loading');
					for (var error in data.errors) {
						if ({}.hasOwnProperty.call(data.errors, error)) {
							var alert = errorTemplate.clone();
							$(alert)
								.removeClass('hidden')
								.children('span')
								.text(data.errors[error]);
							$('.error-content').append(alert);
						}
					}
				}
			}
		});
	}

	upload(e) {
		e.preventDefault();
		e.stopPropagation();

		if (this.files.length > 0) {
			var xhr = new XMLHttpRequest();
			var file = this.files.shift();
			var config = this.$context.locator.resolve('config');
			var authKey = this.$context.cookie.get('auth_key');
			var url = `${config.api.url}upload/image?access-token=${authKey}`;

			xhr.upload.onprogress = function(e) {
				var val, bar;
				var done = e.position || e.loaded;
				var total = e.totalSize || e.total;
				var percent = Math.floor(done / total * 100);

				if (percent >= 100) {
					bar = document.querySelector(`div.percent[data-uuid="${file.uuid}"]`);
					bar.style.display = 'block';
					// bar.nextElementSibling.style.display = 'none';
					// bar.innerHTML = '<i class="fa fa-check"></i>';
				}
			};

			// обработчики успеха и ошибки
			// если status == 200, то это успех, иначе ошибка
			xhr.onload = xhr.onerror = function() {
				if (this.status === 200) {
					var data = JSON.parse(this.response);
					var input = document.createElement('input');
					input.type = 'hidden';
					input.name = 'images[]';
					input.value = data.content.id;
					input.dataset.uuid = file.uuid;
					document.getElementById('addForm').appendChild(input);
				}
			};

			var formData = new FormData();
			formData.append('image', file);
			xhr.open('POST', url, true);
			xhr.send(formData);
			this.upload(e);
		}
	}

	prepare(e) {
		var files = [];
		for (var i = 0; i < e.target.files.length; i++) {
			if (e.target.files[i].type === 'image/jpeg' || e.target.files[i].type === 'image/png') {
				files.push(e.target.files.item(i));
			} else {
				continue;
			}
		}
		this.readFiles(files);
	}

	clear(e) {
		e.preventDefault();

		var currentElement, i;

		if (e.target.tagName === 'I') {
			currentElement = $(e.target).parent('button');
		} else {
			currentElement = $(e.target);
		}

		var uuid = currentElement.data('uuid');
		var input = document.querySelector(`input[data-uuid="${uuid}"]`);

		for (i in this.files) {
			if (this.files[i].uuid === uuid) {
				this.files.splice(i, 1);
				break;
			}
		}

		currentElement.parent().fadeOut(600, () => {
			$(currentElement).remove();
		});

		if (input !== null) {
			input.remove();
		}
	}

	storeFile(file, uuid) {
		var i = this.files.push(file) - 1;
		this.files[i].uuid = uuid;
	}

	readFiles(files) {
		if (files.length > 0) {
			var reader = new FileReader();
			var file = files.shift();
			var self = this;
			var canvas = document.createElement('canvas');
			canvas.width = 150;
			canvas.height = 150;
			var context = canvas.getContext('2d');
			var img = new Image();

			reader.onload = function(e) {
				img.onload = function() {
					var sx, sy, sw, sh, dw, dh, dx, dy, blob, uuid;

					if (img.width > img.height) {
						sx = (img.width - img.height) / 2;
						sy = 0;
						sw = img.height;
						sh = img.height;
					} else {
						sx = 0;
						sy = (img.height - img.width) / 2;
						sw = img.width;
						sh = img.width;
					}
					dx = 0;
					dy = 0;
					dw = 150;
					dh = 150;
					context.drawImage(img, sx, sy, sw, sh, dx, dy, dw, dh);

					var base64 = canvas.toDataURL(file.type);
					var base64result = base64.substr(base64.indexOf(',') + 1);
					var preview = document.getElementById('imageUploadPreview');

					if (file.type === 'image/jpeg' || file.type === 'image/png') {
						blob = URL.createObjectURL(self.b64toBlob(base64result, file.type));
						uuid = blob.match(/[a-f\d-]{35}/);

						self.storeFile(file, uuid);

						preview.innerHTML = `${preview.innerHTML}
						<div class="preview-img upload-img">
							<div class="percent" data-uuid="${uuid}">
								<i class="fa fa-check"></i>
							</div>
							<button class="clear" id="clearButton" data-uuid="${uuid}">
								<i class="fa fa-close"></i>
							</button>
							<img style="width:150px" src="${blob}">
						</div>`;
					}
					self.readFiles(files);
				};
				img.src = e.target.result;
			};
			reader.readAsDataURL(file);
		} else {
		//		finishedReadingFiles() // no more files to read
		}
	}

	b64toBlob(b64Data, contentType, sliceSize) {
		contentType = contentType || '';
		sliceSize = sliceSize || 512;
		var byteCharacters = atob(b64Data);
		var byteArrays = [];

		for (var offset = 0; offset < byteCharacters.length; offset += sliceSize) {
			var slice = byteCharacters.slice(offset, offset + sliceSize);

			var byteNumbers = new Array(slice.length);
			for (var i = 0; i < slice.length; i++) {
				byteNumbers[i] = slice.charCodeAt(i);
			}

			var byteArray = new Uint8Array(byteNumbers);
			byteArrays.push(byteArray);
		}

		return new Blob(byteArrays, {type: contentType});
	}

	validateForm() {
		var images, location, info;
		images = false;
		location = false;
		info = false;

		if (this.files.length !== 0) {
			$('#uploadBtn').removeClass('disabled').parent().tooltip('destroy');
		} else {
			$('#uploadBtn').addClass('disabled', true).parent().tooltip();
		}

		if ($('#addForm input[name="images[]"]').length > 0) {
			$('#uploadCheck').removeClass('hidden');
			images = true;
		}

		if ($('#coordinatesLat').val() !== '' && $('#coordinatesLng').val() !== '') {
			$('#locationCheck').removeClass('hidden');
			location = true;
		}

		if ($('#userTitle').val() !== '') {
			$('#infoCheck').removeClass('hidden');
			info = true;
		}

		if (images && location && info) {
			$('#addBtn').removeClass('disabled').parent().tooltip('destroy');
		} else {
			$('#addBtn').addClass('disabled', true).parent().tooltip();
		}
	}

}

module.exports = PlacemarkAdd;

