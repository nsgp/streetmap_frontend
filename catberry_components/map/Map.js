'use strict';

/*
 * This is a Catberry Cat-component file.
 * More details can be found here
 * http://catberry.org/documentation#cat-components-interface
 */

class Map {

	/**
	 * Creates a new instance of the "map" component.
	 */
	constructor() {

	}

	/**
	 * Gets a data context for the template engine.
	 * This method is optional.
	 * @returns {Promise<Object>|Object|null|undefined} The data context for the template engine.
	 */
	render() {

	}

	/**
	 * Returns event binding settings for the component.
	 * This method is optional.
	 * @returns {Promise<Object>|Object|null|undefined} The binding settings or nothing.
	 */
	bind() {
		return this.$context.getStoreData()
			.then((data) => {
				this.initMapAction(data);
				return {
					click: {
						'button#coordinatesChoose': this.choose,
						'button#coordinatesCancel': this.cancel
					}
				};
			});
	}

	/**
	 * Clans everything up. The events have been set by .bind() method are cleaned automatically.
	 * This method is optional.
	 * @returns {Promise|undefined} Promise or finished work or nothing.
	 */
	unbind() {

	}

	setMap(map) {
		this.map = map;
	}

	getMap() {
		if ({}.hasOwnProperty.call(this, 'map')) {
			return this.map;
		}
		return false;
	}

	choose(e) {
		e.preventDefault();
		var dgMap = this.getMap();

		if (dgMap !== false) {
			var center = dgMap.getCenter();
			document.querySelector('#coordinatesLat').value = center.lat;
			document.querySelector('#coordinatesLng').value = center.lng;
			$('#map-chooser, #map-dialog').fadeOut(600);
			this.offcanvasShow();
		}
	}

	cancel(e) {
		$('#map-chooser, #map-dialog').fadeOut(600);
		this.offcanvasShow();
	}

	setLocationHash(location) {
		var dgMap = this.getMap();

		if (dgMap !== false) {
			var zoom = dgMap.getZoom();
			var center = dgMap.getCenter();
			var lat = center.lat.toFixed(4);
			var lng = center.lng.toFixed(4);
			var hash = `${zoom}\/${lat}\/${lng}`;

			if (typeof location !== 'undefined') {
				this.$context.redirect(`${location}#${hash}`);
			} else {
				this.$context.locator.resolve('window').location.hash = `#${hash}`;
				this.$context.location.fragment = hash;
			}
		}
	}

	getMapCenter() {
		var center = {};
		var fragment = this.$context.location.fragment;

		if (fragment && (fragment = fragment.split('/')) && fragment.length === 3) {
			center.center = fragment.splice(1, 2);
			center.zoom = fragment[0];
		} else {
			center.center = [54.98, 82.89];
			center.zoom = 11;
		}

		return center;
	}

	offcanvasShow() {
		if (this.$context.isBrowser) {
			if (!$('.navmenu').hasClass('in')) {
				$('[data-toggle=offcanvas]').trigger('click.bs.offcanvas.data-api');
			}
		}
	}

	offcanvasHide() {
		if (this.$context.isBrowser) {
			if ($('.navmenu').hasClass('in')) {
				$('[data-toggle=offcanvas]').trigger('click.bs.offcanvas.data-api');
			}
		}
	}

	bindElement(event, selector, callback) {
		$('body').on(event, selector, callback);
	}

	toogleCoordinatesChooseButton(zoom, maxZoom) {
		if (zoom === maxZoom) {
			$('#mapCoordinatesChoose').prop('disabled', false).parent().tooltip('destroy');
		} else {
			$('#mapCoordinatesChoose').prop('disabled', true).parent().tooltip();
		}
	}

	initMapAction(data) {
		if (this.$context.isBrowser) {
			var dgMap, marker;
			var self = this;

			DG.then((data) => {
					// загрузка модуля кластеризации
				return DG.plugin('http://2gis.github.io/mapsapi/vendors/Leaflet.markerCluster/leaflet.markercluster-src.js');
			}).then(() => {
					// загрузка модуля переопределения кнопок
				return DG.plugin('/static/js/2gis/visual.js');
			}).then(() => {
				// Создание карты
				var center = self.getMapCenter();
				var markers = DG.markerClusterGroup({showCoverageOnHover: false});
				var icon = {
					icon: DG.icon({
						iconUrl: '/static/img/default/marker.svg',
						iconSize: [34, 34]
					})
				};

				dgMap = DG.map('map-container', {
					center: center.center,
					zoom: center.zoom,
					zoomControl: false,
					fullscreenControl: false
				}).on('dragend zoomend', (e) => {
					self.setLocationHash();
				}).on('locationfound locationerror', (e) => {
					if (e.type === 'locationfound') {
						dgMap.setView(e.latlng, 18);
					}
				}).on('zoomend', (e) => {
					self.toogleCoordinatesChooseButton(e.target._animateToZoom, e.target._layersMaxZoom);
				});

				this.setMap(dgMap);

				data.placemarks.forEach((placemark) => {
					marker = DG.marker([placemark.coordinates.lat, placemark.coordinates.lng], icon);

					marker.on('click', () => {
						self.setLocationHash(`/placemark/show/${placemark._id.$id}`);
						self.offcanvasShow();
					});

					marker.addTo(markers);
				});

				dgMap.addLayer(markers);
				dgMap.visual.enable();
			}).then(() => {
				self.setLocationHash();
			});
		}
	}

}

module.exports = Map;

