'use strict';

const WrapperComponentBase = require('../../lib/WrapperComponentBase');

/*
 * This is a Catberry Cat-component file.
 * More details can be found here
 * http://catberry.org/documentation#cat-components-interface
 */

class Login extends WrapperComponentBase {

	/**
	 * Creates a new instance of the "login" component.
	 */
	constructor() {
		super();
	}

	/**
	 * Gets a data context for the template engine.
	 * This method is optional.
	 * @returns {Promise<Object>|Object|null|undefined} The data context for the template engine.
	 */
	render() {
		return this.$context.getStoreData();
	}

	/**
	 * Returns event binding settings for the component.
	 * This method is optional.
	 * @returns {Promise<Object>|Object|null|undefined} The binding settings or nothing.
	 */
	bind() {
		return {
			submit: {
				'form#login-form': this.loginAction
			},
			click: {
				'#user_logout': super.logoutAction
			}
		};
	}

	/**
	 * Clans everything up. The events have been set by .bind() method are cleaned automatically.
	 * This method is optional.
	 * @returns {Promise|undefined} Promise or finished work or nothing.
	 */
	unbind() {

	}

	loginAction(e) {
		super.authAction(e, 'login-action');
	}
}

module.exports = Login;

