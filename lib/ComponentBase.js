'use strict';

class ComponentBase {

	/**
	 * Creates a new instance of the ComponentBase.
	 */
	constructor() {

	}

	/**
	 * Gets a data context for the template engine.
	 * This method is optional.
	 * @returns {Promise<Object>|Object|null|undefined} The data context for the template engine.
	 */
	render() {

	}

	/**
	 * Returns event binding settings for the component.
	 * This method is optional.
	 * @returns {Promise<Object>|Object|null|undefined} The binding settings or nothing.
	 */
	bind() {

	}

	/**
	 * Clans everything up. The events have been set by .bind() method are cleaned automatically.
	 * This method is optional.
	 * @returns {Promise|undefined} Promise or finished work or nothing.
	 */
	unbind() {

	}

	logoutAction(e) {
		e.preventDefault();
		e.stopPropagation();
		this.$context.sendBroadcastAction('logout-action', {});
	}
}

module.exports = ComponentBase;
