'use strict';

class WrapperComponentBase {

	/**
	 * Creates a new instance of the WrapperComponentBase.
	 */
	constructor() {
		this.offcanvasShow();
		const handlebars = this.$context.locator.resolve('handlebars');
		handlebars.registerHelper('if_eq', function(a, b, opts) {
			if (a === b) {
				return opts.fn(this);
			}
			return opts.inverse(this);
		});
	}

	/**
	 * Gets a data context for the template engine.
	 * This method is optional.
	 * @returns {Promise<Object>|Object|null|undefined} The data context for the template engine.
	 */
	render() {

	}

	/**
	 * Returns event binding settings for the component.
	 * This method is optional.
	 * @returns {Promise<Object>|Object|null|undefined} The binding settings or nothing.
	 */
	bind() {

	}

	/**
	 * Clans everything up. The events have been set by .bind() method are cleaned automatically.
	 * This method is optional.
	 * @returns {Promise|undefined} Promise or finished work or nothing.
	 */
	unbind() {

	}

	offcanvasShow() {
		if (this.$context.isBrowser) {
			// $('.navmenu').not('.in').offcanvas('show');
			if (!$('.navmenu').hasClass('in')) {
				$('[data-toggle=offcanvas]').trigger('click.bs.offcanvas.data-api');
			}
		}
	}

	serializeObject(form) {
		var els = form.elements;
		var obj = {};

		for (var i = 0; i < els.length; i += 1) {
			var el = els[i];
			var t = el.type;
			var n = el.name;
			var v = el.value;
			var ty = [
				'checkbox',
				'hidden',
				'password',
				'radio',
				'select-one',
				'text',
				'email',
				'textarea'
			];

			if (ty.indexOf(t) > -1) {
				if ({}.hasOwnProperty.call(obj, n)) {
					if (Array.isArray(obj[n])) {
						obj[n].push(v);
					} else {
						obj[n] = [obj[n], v];
					}
				} else {
					obj[n] = v;
				}
			}
		}
		return obj;
	}

	logoutAction(e) {
		e.preventDefault();
		e.stopPropagation();
		this.$context.sendAction('logout-action');
	}

	authAction(e, action) {
		e.preventDefault();
		e.stopPropagation();

		var button = e.target.querySelector('[type=submit]');
		button.setAttribute('disabled', true);
		$(e.target).addClass('form-loading');

		var result = this.$context.sendAction(action, this.serializeObject(e.target));
		result.then(data => {
			if (!data.status) {
				if (data.hasOwnProperty('errors')) {
					$('.error-content').html('');
					button.removeAttribute('disabled');
					var errorTemplate = $('.alert');
					$(e.target).removeClass('form-loading');
					for (var error in data.errors) {
						if ({}.hasOwnProperty.call(data.errors, error)) {
							var alert = errorTemplate.clone();
							$(alert)
								.removeClass('hidden')
								.children('span')
								.text(data.errors[error]);
							$('.error-content').append(alert);
						}
					}
				}
			}
		});
	}
}

module.exports = WrapperComponentBase;
