# Street Art Guide frontend (Catberry)

![Catberry](https://raw.githubusercontent.com/catberry/catberry/master/docs/images/logo.png)

## How to use

This application only shows "Hello, world!" page.

First of all, it is needed to install dependencies:

```
npm install -- production
```

Then to start in `debug` mode without script minification and with watching for changes:

```
npm run debug
```

To start in `release` mode:

```
npm start
```

## Application configuration
Create /config/api.json

```
{
  "api": {
    "url": "http://example.com/"
  }
}
```


## Server configuration

Create a Directory for the Certificate
```
sudo mkdir /etc/nginx/ssl
cd /etc/nginx/ssl
```
Create the Server Key and Certificate Signing Reques
```
sudo openssl genrsa -des3 -out server.key 2048
sudo openssl req -new -key server.key -out server.csr
```
Remove the Passphrase
```
sudo cp server.key server.key.org
sudo openssl rsa -in server.key.org -out server.key

```
Sign your SSL Certificate
```
sudo openssl x509 -req -days 365 -in server.csr -signkey server.key -out server.crt
```

```
upstream frontend {
	server localhost:3000;
}

server {
	listen 443;
	server_name yourdomain.com;

	ssl on;
	ssl_certificate /etc/nginx/ssl/server.crt;
	ssl_certificate_key /etc/nginx/ssl/server.key; 

	location / {
		proxy_set_header X-Real-IP $remote_addr;
		proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
		proxy_set_header Host $http_host;
		proxy_set_header X-NginX-Proxy true;
		proxy_pass  http://frontend;
		proxy_redirect off;
	}
}

server {
	listen 80;
	server_name yourdomain.com;

	# pass the request to the node.js server with the correct headers
	# and much more can be added, see nginx config options
	location / {
		proxy_set_header X-Real-IP $remote_addr;
		proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
		proxy_set_header Host $http_host;
		proxy_set_header X-NginX-Proxy true;
		proxy_pass http://frontend/;
		proxy_redirect off;
	}
 }
```

## Wrapper components

Components that dynamicly load in cat-wrapper component and need sidebar toggle in, when page loaded.
They grouped by directories named `group`-`name` and loads by request /`group`/`name`.

In a catberry_stores/Wrapper.js:

```javascript
const ALLOWED_ROUTES = {
  auth: {
    blank: true,
    login: true,
    registration: true, // component names
    store: 'Auth', // component store
    default: 'blank' // default component-name if undefined
  }
};
const DEFAULT_GROUP = 'auth'; // default component-group if undefined
```

```javascript
'use strict';

const WrapperComponentBase = require('../../lib/WrapperComponentBase');

class Component extends WrapperComponentBase {

  constructor() {
    super();
  }

  render() {

  }

  bind() {

  }

  unbind() {

  }
}

module.exports = Component;
```

##Eslint

You should run it using `make lint` before committing to the repo. If you need
to fix indentations automatically then use `make lint-fix`.


[Catberry](https://github.com/catberry/catberry)

Denis Rechkunov <denis.rechkunov@gmail.com>
