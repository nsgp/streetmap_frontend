DG.Visual = DG.Handler.extend({

    _lastFirms: DG.layerGroup(),

    _toggleFullScreen: function(el) {
        if (!document.fullscreenElement &&	// alternative standard method
            !document.mozFullScreenElement &&
            !document.webkitFullscreenElement) {  // current working methods
            if (el.requestFullscreen) {
                el.requestFullscreen();
            } else if (el.mozRequestFullScreen) {
                el.mozRequestFullScreen();
            } else if (el.webkitRequestFullscreen) {
                el.webkitRequestFullscreen(Element.ALLOW_KEYBOARD_INPUT);
            }
        } else {
            if (document.cancelFullScreen) {
                document.cancelFullScreen();
            } else if (document.mozCancelFullScreen) {
                document.mozCancelFullScreen();
            } else if (document.webkitCancelFullScreen) {
                document.webkitCancelFullScreen();
            }
        }
    },

    addHooks: function() {
        var sagZoomControl = DG.Control.extend({
            options: {
                position: 'topleft'
            },

            onAdd: function (map) {
                // Создает контейнер элемента управления с определенным именем класса
                var zoomContainer = DG.DomUtil.create('div', 'sag-zoom-control'),
                    buttonPlus = DG.DomUtil.create('button', 'btn btn-primary btn-sm', zoomContainer),
                    buttonMinus = DG.DomUtil.create('button', 'btn btn-primary btn-sm', zoomContainer),
                    iconPlus = DG.DomUtil.create('i', 'fa fa-plus', buttonPlus),
                    iconMinus = DG.DomUtil.create('i', 'fa fa-minus', buttonMinus);

                buttonPlus.id = 'zoom-in';
                buttonPlus.onclick = function() {
                    map.zoomIn();
                };
                buttonMinus.id = 'zoom-out';
                buttonMinus.onclick = function() {
                    map.zoomOut();
                };
                // ... инициализирует другие DOM элементы, добавляет обработчики событий и т.п.
                return zoomContainer;
            }
        });

        var toggleFullScreen = this._toggleFullScreen;
        var sagFullscreenControl = DG.Control.extend({
            options: {
                position: 'bottomleft'
            },

            onAdd: function (map) {
                // создает контейнер элемента управления с определенным именем класса
                var fullscreenContainer = DG.DomUtil.create('div', 'sag-fullscreen-control'),
                    buttonFullscreen = DG.DomUtil.create('button', 'btn btn-primary btn-sm', fullscreenContainer),
                    iconFullscreen = DG.DomUtil.create('i', 'fa fa fa-arrows-alt', buttonFullscreen);

                buttonFullscreen.id = 'fullscreen',
                    buttonFullscreen.onclick = function() {
                        toggleFullScreen(map._container);
                    };

                // ... инициализирует другие DOM элементы, добавляет обработчики событий и т.п.

                return fullscreenContainer;
            }
        });

        this._map.addControl(new sagZoomControl());
        this._map.addControl(new sagFullscreenControl());

        //this._map.on('click', this._searchFirms, this);
    },

    removeHooks: function() {
    //    this._map.off('click', this._searchFirms, this);
    },


    _searchFirms: function(e) { // (MouseEvent)
        var latlng = e.latlng.wrap();

        DG.ajax({
            url: 'http://catalog.api.2gis.ru/2.0/search',
            data: {
                what: 'магазин',
                point: latlng.lng + ',' + latlng.lat,
                radius: 500,
                page_size: 50,
                type: 'filial',
                key: '12345678'
            },
            success: DG.bind(this._showFirms, this),
            error: DG.bind(this._logError, this)
        });
    },

    _showFirms: function(data) { // (Object)
        var firms, marker;

        if (data.response.code > 200) {
            this._logError(data);
            return;
        }

        this._lastFirms.clearLayers();

        firms = data.result.data;
        firms.forEach(function(firmInfo) {
            marker = DG.marker([firmInfo.geo.lat, firmInfo.geo.lon]);
            marker.bindPopup(firmInfo.firm.name);
            marker.addTo(this._lastFirms);
        }, this);

        this._lastFirms.addTo(this._map);
    },

    _logError: function(data) {
        console.log('Error: ', data);
    }
});

DG.Map.addInitHook('addHandler', 'visual', DG.Visual);