'use strict';

/*
 * This is a Catberry Cat-component file.
 * More details can be found here
 * http://catberry.org/documentation#stores-interface
 */

class Auth {

	/**
	 * Creates a new instance of the "Auth" store.
	 * @param {ServiceLocator} locator The service locator for resolving dependencies.
	 */
	constructor(locator) {

		// In case you have the UHR plugin registered
		this._uhr = locator.resolve('uhr');
		this._config = locator.resolve('config');

		/**
		* Current lifetime for data (in milliseconds) which the store is responsible for.
		* @type {number} Lifetime in milliseconds.
		*/
		this.$lifetime = 1;
	}

	load() {
		var status, content;
		var location = this.$context.location;
		var authToken = this.$context.cookie.get('auth_key');

		const options = {
			headers: {
				Authorization: `Bearer ${authToken}`
			}
		};

		return this._uhr.get(`${this._config.api.url}user`, options)
			.then((data) => {
				content = data.content;
				if (data.status.code === 200) {
					status = true;
					return {
						status,
						content,
						location
					};
				}
				status = false;
				return {
					status,
					location
				};
			});
	}

	handleUpdateAction() {
		this.$context.changed();
	}

	handleLogoutAction() {
		var context = this.$context;
		var key = 'auth_key';
		var value = 'false';
		var path = '/';
		var domain = context.location.authority.host;
		context.cookie.set({
			key,
			value,
			path,
			domain
		});
		context.sendBroadcastAction('update-action', {});
		context.redirect('/auth/login');
	}

	handleLoginAction(data) {
		return this._handleAuthAction(data, 'auth/login');
	}

	handleRegistrationAction(data) {
		return this._handleAuthAction(data, 'auth/registration');
	}

	_handleAuthAction(data, urlPath) {
		// Here you can call this.$context.changed() if you know
		// that remote data has been changed.
		// Also, you can have other actions and handle methods like this, just define a method.
		var timeout = 3000;
		const options = {
			timeout,
			data
		};

		var context = this.$context;

		return this._uhr.post(this._config.api.url + urlPath, options)
			.then((data) => {
				if (data.content.status === true) {
					context.cookie.set({
						key: 'auth_key',
						value: data.content.token,
						path: '/',
						domain: context.location.authority.host
					});
					context.sendBroadcastAction('update-action', {});
					context.redirect('/');
				}
				return data.content;
			});
	}
}

module.exports = Auth;

