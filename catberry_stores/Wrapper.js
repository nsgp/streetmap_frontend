'use strict';

const ALLOWED_ROUTES = {
	auth: {
		index: true,
		login: true,
		registration: true,
		store: 'Auth',
		default: 'index'
	},
	placemark: {
		add: true,
		show: true,
		store: 'Placemark',
		default: 'show'
	},
	user: {
		profile: true,
		settings: true,
		store: 'User',
		default: 'profile'
	}
};
const DEFAULT_GROUP = 'auth';

/*
 * This is a Catberry Cat-component file.
 * More details can be found here
 * http://catberry.org/documentation#stores-interface
 */

class Wrapper {

	/**
	 * Creates a new instance of the "Wrapper" store.
	 * @param {ServiceLocator} locator The service locator for resolving dependencies.
	 */
	constructor(locator) {

		// In case you have the UHR plugin registered
		// this._uhr = locator.resolve('uhr');

		/**
		* Current lifetime for data (in milliseconds) which the store is responsible for.
		* @type {number} Lifetime in milliseconds.
		*/
		this.$lifetime = 60000;
	}

	load() {
		// Here you can do any HTTP requests using this._uhr
		// or another universal HTTP request implementation.
		// Please read details here https://github.com/catberry/catberry-uhr.
		let component, store;
		let group = this.$context.state.group;
		let name = this.$context.state.name;

		if (!group) {
			group = DEFAULT_GROUP;
		}

		group = group.toLowerCase();

		if (!ALLOWED_ROUTES.hasOwnProperty(group)) {
			component = `${DEFAULT_GROUP}-${ALLOWED_ROUTES[DEFAULT_GROUP].default}`;
			store = ALLOWED_ROUTES[DEFAULT_GROUP].store;
		} else {

			if (!name || !ALLOWED_ROUTES[group].hasOwnProperty(name)) {
				name = ALLOWED_ROUTES[group].default;
			}

			name = name.toLowerCase();
			component = `${group}-${name}`;
			store = ALLOWED_ROUTES[group].store;
		}

		return {
			component,
			store
		};
	}

	handleSomeAction() {
		// Here you can call this.$context.changed() if you know
		// that remote data has been changed.
		// Also, you can have other actions and handle methods like this, just define a method.
	}
}

module.exports = Wrapper;

