'use strict';

/*
 * This is a Catberry Cat-component file.
 * More details can be found here
 * http://catberry.org/documentation#stores-interface
 */

class User {

	/**
	 * Creates a new instance of the "Placemark" store.
	 * @param {ServiceLocator} locator The service locator for resolving dependencies.
	 */
	constructor(locator) {

		// In case you have the UHR plugin registered
		this._uhr = locator.resolve('uhr');
		this._config = locator.resolve('config');

		/**
		* Current lifetime for data (in milliseconds) which the store is responsible for.
		* @type {number} Lifetime in milliseconds.
		*/
		this.$lifetime = 1000;
	}

	load() {
		// Here you can do any HTTP requests using this._uhr
		// or another universal HTTP request implementation.
		// Please read details here https://github.com/catberry/catberry-uhr.

		var status, settings;
		var id = this.$context.state.id;
		var url = this._config.api.url;
		var authToken = this.$context.cookie.get('auth_key');

		const options = {
			headers: {
				Authorization: `Bearer ${authToken}`
			}
		};



		return this._uhr.get(`${this._config.api.url}settingslists`, options)
			.then((data) => {
				settings = data.content;
				if (data.status.code === 200) {
					status = true;
					for(var i in settings){
						if(settings[i].settingType.setting_type=='FILE' && settings[i].settings!=false) {
							settings[i].settings.value = url+settings[i].settings.value.c_o; 				
						}
					}

					return {
						status,
						settings
					};
				}
				status = false;
				return {
					status,
					settings
				};
			});

		// http://streetmap.backend:808/settingslists?access-token=fnj7ipLJRsE4Mp46jHAsAJlW6o0FWLvsh8q7LWIwtP9Yq4yZFzHGcXVxp2OiyHiuf0xr39_6TPNY56Hql6beH
	}

	handleSavesettingsAction(data) {
		var authToken = this.$context.cookie.get('auth_key');

		const options = {
			headers: {
				Authorization: `Bearer ${authToken}`
			},
			data
		};

		return this._uhr.post(`${this._config.api.url}settingslists`, options)
			.then((data) => {
				if (data.status.code === 200) {
					return data.content;
				}
				return data.content;
			});

	}
}

module.exports = User;

