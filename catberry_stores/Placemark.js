'use strict';

/*
 * This is a Catberry Cat-component file.
 * More details can be found here
 * http://catberry.org/documentation#stores-interface
 */

class Placemark {

	/**
	 * Creates a new instance of the "Placemark" store.
	 * @param {ServiceLocator} locator The service locator for resolving dependencies.
	 */
	constructor(locator) {

		// In case you have the UHR plugin registered
		this._uhr = locator.resolve('uhr');
		this._config = locator.resolve('config');

		/**
		* Current lifetime for data (in milliseconds) which the store is responsible for.
		* @type {number} Lifetime in milliseconds.
		*/
		this.$lifetime = 60000;
	}

	load() {
		// Here you can do any HTTP requests using this._uhr
		// or another universal HTTP request implementation.
		// Please read details here https://github.com/catberry/catberry-uhr.

		var status, content;
		var id = this.$context.state.id;
		var url = this._config.api.url;
		var comments = {
			0: {
				username: 'Иван Иванов',
				text: 'В интернете все такое смешное! Как же здесь здорово!',
				avatar: '/static/img/default/user-image-placeholder-50.jpg'
			},
			1: {
				username: 'Петр Петров',
				text: 'Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque ante sollicitudin commodo. Cras purus odio, vestibulum in vulputate at, tempus viverra turpis. Fusce condimentum nunc ac nisi vulputate fringilla. Donec lacinia congue felis in faucibus.',
				avatar: '/static/img/default/user-image-placeholder-50.png'
			}
		};

		return this._uhr.get(`${this._config.api.url}placemarks/${id}?expand=img&fields=attributes`)
			.then((data) => {
				content = data.content;
				if (data.status.code === 200) {
					status = true;
					return {
						status,
						content,
						url,
						id,
						comments
					};
				}
				status = false;
				return {
					status,
					content
				};
			});
	}

	handleSomeAction() {
		// Here you can call this.$context.changed() if you know
		// that remote data has been changed.
		// Also, you can have other actions and handle methods like this, just define a method.
	}

	handleAddAction(data) {
		// Here you can call this.$context.changed() if you know
		// that remote data has been changed.
		// Also, you can have other actions and handle methods like this, just define a method.
		var authKey = this.$context.cookie.get('auth_key');
		var timeout = 3000;
		const options = {
			timeout,
			data
		};

		return this._uhr.post(`${this._config.api.url}placemarks?access-token=${authKey}`, options)
			.then((data) => {
				return data.content;
			});
	}
}

module.exports = Placemark;

