'use strict';

class Main {

	/**
	 * Loads data from somewhere.
	 * @returns {Object} Data object.
	 */
	load() {
		return {
			appname: 'Street Art Guide <sup>&alpha;</sup>'
		};
	}
}

module.exports = Main;

