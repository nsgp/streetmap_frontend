'use strict';

/*
 * This is a Catberry Cat-component file.
 * More details can be found here
 * http://catberry.org/documentation#stores-interface
 */

class Cities {

	/**
	 * Creates a new instance of the "Cities" store.
	 * @param {ServiceLocator} locator The service locator for resolving dependencies.
	 */
	constructor(locator) {
		this._config = locator.resolve('config');
		this._uhr = locator.resolve('uhr');
		this.$lifetime = 160000;
		// In case you have the UHR plugin registered
		// this._uhr = locator.resolve('uhr');

		/**
		* Current lifetime for data (in milliseconds) which the store is responsible for.
		* @type {number} Lifetime in milliseconds.
		*/
		this.$lifetime = 60000;
	}

	load() {
		var status, content;

		return this._uhr.get(`${this._config.api.url}/region/all`)
			.then((data) => {
				status = true;
				content = data.content;
				if (data.status.code === 200) {
					return {
						status,
						content
					};
				}
				status = false;
				return {
					status,
					content
				};
			});
	}

	handleSomeAction() {
		// Here you can call this.$context.changed() if you know
		// that remote data has been changed.
		// Also, you can have other actions and handle methods like this, just define a method.
	}
}

module.exports = Cities;

