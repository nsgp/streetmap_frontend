'use strict';

/*
 * This is a Catberry Cat-component file.
 * More details can be found here
 * http://catberry.org/documentation#stores-interface
 */

class Menu {

	/**
	 * Creates a new instance of the "Menu" store.
	 * @param {ServiceLocator} locator The service locator for resolving dependencies.
	 */
	constructor(locator) {

		// In case you have the UHR plugin registered
		this._uhr = locator.resolve('uhr');
		this._config = locator.resolve('config');

		/**
		* Current lifetime for data (in milliseconds) which the store is responsible for.
		* @type {number} Lifetime in milliseconds.
		*/
		this.$lifetime = 60000;
	}

	getLinkLists() {
		return {

/*			'common': {
				'common link_title': '/test/link_title',
				'common link_title2': '/test/link_title2',
			},
			'user': {
				'user link_title': '/test/user_title',
			}*/
		};
	}

	load() {
		// Here you can do any HTTP requests using this._uhr
		// or another universal HTTP request implementation.
		// Please read details here https://github.com/catberry/catberry-uhr.
		var status, content;
		var signature = false;

		var links = this.getLinkLists();
		var authToken = this.$context.cookie.get('auth_key');

		const options = {
			headers: {
				Authorization: `Bearer ${authToken}`
			},
			data: {
				expand: 'settings'
			}
		};

		return this._uhr.get(`${this._config.api.url}user`, options)
			.then((data) => {
				if (data.status.code === 200) {
					status = true;
					content = data.content;


					if(content.settings.length!=0) {

						signature = data.content.settings[1].value;
						var photoProfile = false;
						if(data.content.settings[4] !== undefined && data.content.settings[4].value != false ) {
							photoProfile = `${this._config.api.url}` + data.content.settings[4].value.s;
						}
					}
					return {
						status,
						content,
						photoProfile,
						signature,
						links
					};
				}
				status = false;
				return {
					status,
					links
				};
			});
	}

	handleUpdateAction() {
		this.$context.changed();
	}

	handleSomeAction() {
		// Here you can call this.$context.changed() if you know
		// that remote data has been changed.
		// Also, you can have other actions and handle methods like this, just define a method.
	}
}

module.exports = Menu;

